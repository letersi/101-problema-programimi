### Kërkesa

Çdo ditë Majko shkon në punë me autobus, ku pret një biletë.  Çdo
biletë ka një kod me shkronja latine. Majko beson se dita do ti shkojë
mbarë nëse kodi i biletës ka vetëm 2 shkronja alternuese, si
p.sh. **xyxyxy...** ku **x!=y**.

Bëni një program që merr kodin e biletës dhe nxjerr "YES" nëse është
alternues, dhe "NO" në të kundërt.

Referenca: https://www.codechef.com/problems/TICKETS5

#### Shembull

```
$ cat input.txt
2
ABABAB
ABC

$ python3 prog.py < input.txt
YES
NO
```

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    S = input()
    if S[0] == S[1]:
        print('NO')
    else:
        for i in range(2, len(S)):
            if S[i] != S[i % 2]:
                print('NO')
                break
        else:
            print('YES')
```

### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    S = input()
    if S[0] == S[1]:
        print('NO')
    else:
        i = 2
        while i < len(S):
            if S[i] != S[i - 2]:
                print('NO')
                break
            i += 1
        else:
            print('YES')
```

### Detyra

Një ari i vogël polar ka qejf të hajë biskota dhe të pijë qumësht. Për
këtë arsye shkon shpesh në kuzhinë. Ai qëndron në kuzhinë **N**
minuta, dhe çdo minutë ose ha një biskotë, ose pi qumësht. Meqenëse
biskotat janë shumë të ëmbla, prindërit e kanë këshilluar që sa herë
të hajë një biskotë, në minutën tjetër duhet të pijë qumësht.

Na jepet sa minuta ka ndenjur në kuzhinë arushi dhe çfarë ka bërë çdo
minutë: ka ngrënë biskotë apo ka pirë qumësht. Gjeni nëse arushi i ka
zbatuar ose jo këshillat e prindërve, dmth që pas ngrënies së një
biskote ka pirë qumësht.

Referenca: https://www.codechef.com/problems/COOMILK

#### Shembull

```
$ cat input.txt
4
7
cookie milk milk cookie milk cookie milk
5
cookie cookie milk milk milk
4
milk milk milk milk
1
cookie

$ python3 prog.py < input.txt
YES
NO
YES
NO
```
