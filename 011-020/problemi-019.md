### Kërkesa

Bëni një program që e shkruan mbrapsht një numër të dhënë natyror.

Referenca: https://www.codechef.com/problems/FLOW007

#### Shembull

```
$ cat input.txt
4
12345
31203
2123
2300

$ python3 prog.py < input.txt
54321
30213
3212
32
```

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    n = int(input())
    l = []
    while n > 0:
        l.append(n % 10)
        n //= 10
    n1 = 0
    for d in l:
        n1 *= 10
        n1 += d
    print(n1)
```

#### Sqarime

Fillimisht nxjerrim të gjitha shifrat e numrit, duke marrë mbetjen e
pjesëtimit me 10, dhe duke e vazhduar këtë punë me herësin e
pjesëtimit me 10. Këto shifra i ruajmë në një listë.

Pastaj ndërtojmë numrin e ri duke shumëzuar me 10 dhe duke shtuar
shifrat, por sipas radhës së kundërt.

### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    l = list(map(int, list(input())))
    l.reverse()
    n1 = 0
    for d in l:
        n1 *= 10
        n1 += d
    print(n1)
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    l = list(input())
    l.reverse()
    print(int(''.join(l)))
```

#### Sqarime

Funksioni `list(input())` krijon një listë me shkronjat e vargut të lexuar.

Funksioni `join(l)` që thirret te vargu `''`, i bashkon (prapangjit)
këtij vargu të gjitha vargjet që ndodhen në listën `l`.

Para se ta printojmë duhet ta kthejmë në `int`, në mënyrë që të
eliminojmë ndonjë zero fillestare që mund të ketë.

### Detyra

Në një lojë të rregullt tenisi, lojtari që shërben ndërrohet çdo 2
pikë, pavarësisht se kush i shënon.  Chef dhe Cook po e luajnë lojën
pak më ndryshe. Ata vendosën që lojtari i shërbimit të ndërrohet çdo
$`k`$ pikë, pavarësisht se kush i shënon.

Në fillim të lojës, radhën e shërbimit e ka gjithmonë Chef-i. Nqs
dimë që deri tani Chef ka shënuar $`p_1`$ pikë dhe Cook ka shënuar
$`p_2`$ pikë, kush e ka tani radhën e shërbimit?

Referenca: https://www.codechef.com/problems/CHSERVE

#### Shembull

```
$ cat input.txt
3
1 3 2
0 3 2
34 55 2

$ python3 prog.py < input.txt
CHEF
COOK
CHEF
```

Tre numrat që jepen janë $`p_1`$, $`p_2`$ dhe $`k`$.
