### Kërkesa

Mësuesi u ka dhënë nxënësve për të lexuar një libër historik, dhe u ka
kërkuar si detyrë që të shkruajnë emrat e disa prej personaliteteve të
shquar që përmenden në të. Mirëpo kur nxënësit dorëzuan detyrat
mësuesi mbeti i pakënaqur sepse nuk i kishin formatuar emrat siç
duhet.

Një emër mund të ketë tre pjesë, emrin e personit, emrin e babait dhe
mbiemrin.  Nga këto, mbiemri nuk mungon asnjëherë, kurse dy të tjerët
mund edhe të mos shkruhen. Të treja pjesët duhet të fillojnë me
shkronjë të madhe, kurse shkronjat e tjera duhet të jenë të vogla. Dy
pjesët e para duhet të shkruhen me shkurtim, vetëm shkronja e parë dhe
një pikë pas saj.

Bëni një program që lexon disa emra dhe i shkruan siç duhet.

Referenca: https://www.codechef.com/problems/NITIKA

#### Shembull

```
$ cat input.txt
3
gandhi
mahatma gandhI
Mohndas KaramChand gandhi

$ python3 prog.py < input.txt
Gandhi 
M. Gandhi 
M. K. Gandhi 
```

### Zgjidhja

```python
t = int(input())
while t > 0:
    t -= 1
    L = input().split()
    if len(L) == 1:
        print(L[0].capitalize())
    elif len(L) == 2:
        print(L[0][0].upper() + '. ' + L[1].capitalize())
    else:
        print(L[0][0].upper() + '. ' + L[1][0].upper() + '. ' + L[2].capitalize())
```
