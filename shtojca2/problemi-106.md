### Kërkesa

Bëni një program që gjen rrënjën katrore natyrore (të përafërt) të një
numri natyror të dhënë.

Referenca: https://www.codechef.com/problems/FSQRT

#### Shembull

```
$ cat input.txt
3
10
5
10000

$ python3 prog.py < input.txt
3
2
100
```

### Zgjidhja

```python
for _ in range(int(input())):
    n = int(input())
    i = 1
    while i*i <= n:
        i += 1
    print(i)
```
