### Kërkesa

Në një varg **S** me gjatësi **N**, gjeni numrin e treguesve **i**
($`1 <= i <= N-1`$) të tillë që shkronja në vendin **i** është
bashkëtingëllore, kurse shkronja në vendin **i+1** është zanore.
Shkronjat janë nga alfabeti anglisht dhe zanoret janë 'a', 'e', 'i',
'o', 'u'.

Referenca: https://www.codechef.com/problems/CV

#### Shembull

```
$ cat input.txt
3
6
bazeci
3
abu
1
o

$ python3 prog.py < input.txt
3
1
0
```

Në rastin e parë, zanorja 'a' vjen pas bashkëtingëllores 'b', 'e' vjen
pas 'z', dhe 'i' vjen pas 'c', kështu që përgjigja është **3**.

Në rastin e dytë, zanorja 'u' vjen pas bashkëtingëllores 'b', kështu
që përgjigja është **1**.

### Zgjidhja

```python
for _ in range(int(input())):
    n = int(input())
    S = input()
    nr = 0
    V = ['a', 'e', 'i', 'o', 'u']
    for i in range(n-1):
        if S[i] not in V and S[i+1] in V:
            nr += 1
    print(nr)
```
