
### Kërkesa

Në një varg numrash $`a_1, a_2, ... , a_n`$ gjeni vlerën më të vogël
të mundshme të shprehjes $`a_i + a_j`$, ku $`1 \leq i < j \leq n`$.

Referenca: https://www.codechef.com/problems/SMPAIR

#### Shembull

```
$ cat input.txt
1
4
5 1 3 4

$ python3 prog.py < input.txt
4
```

### Zgjidhja

```python
T = int(input())
for t in range(T):
    n = int(input())
    l = list(map(int, input().split()))
    l.sort()
    print(l[0] + l[1])
```

https://tinyurl.com/101-prog-004

#### Sqarime

Te lista `l` lexojmë vargun e numrave. Funksioni `sort()`, i cili
zbatohet te lista, e rendit listën në rendin rritës. Meqenëse lista
e numrave është e renditur, dy të parët janë më të vegjlit, kështu që
shuma e tyre është shuma më e vogël e mundshme.

Vini re që numri i parë i listës e ka indeksin **0** (`l[0]`), numri i
dytë e ka indeksin **1**, e kështu me radhë. Pra numërimi i
elementeve të një liste fillon nga **0**.



### Detyra

Në një varg numrash $`a_1, a_2, ... , a_n`$ gjeni vlerën më të
**madhe** të mundshme të shprehjes $`a_i + a_j`$, ku $`1 \leq i < j
\leq n`$.

#### Shembull

```
$ cat input.txt
1
4
5 1 3 4

$ python3 prog.py < input.txt
9
```
