### Kërkesa

Mamaja i ka blerë Cufit dy shporta me fruta, njëra ka N mollë dhe
tjera ka M portokalle.  Cufit i pëlqejnë edhe mollët edhe portokallet
dhe do që ti ketë në sasi të barabartë. Çdo kokërr mollë ose
portokalle kushton 1 monedhë, dhe Cufi ka K monedha në xhep. Me këto
monedha ai mund të blejë mollë ose portokalle dhe të përpiqet ta bëjë
ndryshimin midis tyre sa më të vogël.

Bëni një program që të gjejë ndryshimin më të vogël të mundshëm që
mund të arrijë Cufi midis mollëve dhe portokalleve.

Referenca: https://www.codechef.com/problems/FRUITS

#### Shembull

```
$ cat input.txt
3
3 4 1
5 2 1
3 4 3

$ python3 prog.py < input.txt
0
2
0
```

Jepen numrat N, M dhe K. Në rastin e parë Cufi mund të blejë një mollë
dhe ti bëjë të barabarta.  Në rastin e dytë mund të blejë vetëm një
portokalle dhe diferenca bëhet 2. Në rastin e tretë mund të blejë 2
mollë dhe një portokalle, duke e bërë diferencën 0.

### Zgjidhja

```python
T = int(input())
for t in range(T):
    n, m, k = map(int, input().split())
    d = abs(n - m)
    print(0) if k > d else print(d - k)
```
