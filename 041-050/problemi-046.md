### Kërkesa

Një listë quhet "listë ylber" nëse plotëson këto kushte:
- $`a_1`$ elementët e parë janë **1**
- $`a_2`$ elementët e tjerë janë **2**
- $`a_3`$ elementët e tjerë janë **3**
- $`a_4`$ elementët e tjerë janë **4**
- $`a_5`$ elementët e tjerë janë **5**
- $`a_6`$ elementët e tjerë janë **6**
- $`a_7`$ elementët e tjerë janë **7**
- $`a_6`$ elementët e tjerë janë **6**
- $`a_5`$ elementët e tjerë janë **5**
- $`a_4`$ elementët e tjerë janë **4**
- $`a_3`$ elementët e tjerë janë **3**
- $`a_2`$ elementët e tjerë janë **2**
- $`a_1`$ elementët e fundit janë **1**
- ku $`a_i`$ është një numër natyror (jo-zero) i çfarëdoshëm
- dhe nuk ka elementë të tjerë në listë përveç këtyre

Bëni një program që gjen nëse një listë numrash është listë ylber.

Referenca: https://www.codechef.com/problems/RAINBOWA

#### Shembull

```
$ cat input.txt
3
19
1 2 3 4 4 5 6 6 6 7 6 6 6 5 4 4 3 2 1
14
1 2 3 4 5 6 7 6 5 4 3 2 1 1
13
1 2 3 4 5 6 8 6 5 4 3 2 1

$ python3 prog.py < input.txt
yes
no
no
```

Rasti i parë i plotëson të gjitha kushtet.

Rasti i dytë ka vetëm një njësh në fillim dhe dy njësha në fund.

Rasti i tretë nuk ka asnjë element **7** dhe ka elemente tepër
(**8**).

### Zgjidhja

```python
for _ in range(int(input())):
    n = int(input())
    L = list(map(int, input().split()))
    i = 0
    j = n-1
    for a in range(1,8):
        if L[i] != a:
            print('no')
            break
        while L[i] == a and L[j] == a and i < j:
            i += 1
            j -= 1
    else:
        if i < j:
            print('no')
        else:
            print('yes')
```

#### Sqarime

Meqenëse elementët e listës duhet të jenë simetrikë në lidhje me
qendrën, përdorim dy treguesa `i` dhe `j`, të cilët nisen prej
fillimit dhe fundit dhe vazhdojnë të ecin deri sa të takohen.
Gjithashtu kontrollojmë nëse elementët e listës marrin me radhë
vlerat nga 1 deri në 7. Mënyra se si e kemi ndërtuar zgjidhjen siguron
që asnjë nga këto vlera të mos mungojë, dhe gjithashtu të mos ketë
vlera të tjera përveç këtyre.

### Detyra

Alisa dhe Beni po bëjnë një lojë me numra. Fillimisht Alisa ka një
numër **A** kurse Beni një numër **B**. Loja ka gjithsej **N** hapa
dhe Alisa me Benin luajnë me radhë. Kur është radha e tij, secili
lojtar e shumëzon numrin e tij me dy. E para luan Alisa.

Bëni një program i cili merr numrat **A**, **B** dhe **N** dhe gjen sa
është herësi i pjesëtimit të numrave në fund të lojës (duke pjesëtuar
më të madhin me më të voglin).

Referenca: https://www.codechef.com/problems/TWONMS

#### Shembull

```
$ cat input.txt
3
1 2 1
3 2 3
3 7 2

$ python3 prog.py < input.txt
1
3
2
```

Në rastin e parë, numrat fillestarë janë (A=1, B=2) dhe loja ka vetëm
1 hap. Në këtë hap Alisa e shumëzon numrin e saj me 2, kështu që në
fund të lojës kemi (A=2, B=2) dhe herësi është 1.

Në rastin e dytë kemi (A=3, B=2) dhe N=3. Pasi luan Alisa, Beni,
Alisa, kemi (A=12, B=4), kështu që herësi është 3.

Në rastin e tretë kemi (A=3, B=7) dhe N=2. Pasi luan Alisa dhe Beni,
kemi (A=6, B=14), kështu që herësi është 2.
