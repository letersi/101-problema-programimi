#!/bin/bash -x
### Fix markdown formating.

cd $(dirname $0)
cd ..

for file in $(find . -name '*.md'); do
    sed -i $file \
        -e '/# Problem/d' \
        -e '/# Kërkesa/ c ### Kërkesa' \
        -e '/# Shembull/ c #### Shembull' \
        -e 's/^#\+ Zgjidhja\(.*\)/### Zgjidhja\1/' \
        -e '/# Sqarime/ c #### Sqarime' \
        -e '/# Detyr/ c ### Detyra' \
        -e 's/^```bash/```/'
done
