

### Kërkesa

Bëni një program që merr 5 numra dhe nxjerr katrorin e tyre (fuqinë e dytë).

#### Shembull

```
$ cat input.txt
13
7
3
11
5

$ python3 p001.py < input.txt
169
49
9
121
25
```

**Shënim:** Komanda `cat` shfaq në ekran përmbajtjen e një skedari.


### Zgjidhja 1

```python
n = int(input())
print(n * n)
n = int(input())
print(n * n)
n = int(input())
print(n * n)
n = int(input())
print(n * n)
n = int(input())
print(n * n)
```

https://tinyurl.com/101-prog-001-1

#### Sqarime

Ne duam të lexojmë një numër, por funksioni `input()` kthen një
*string* (varg me shkronja), kështu që përdorim funksionin `int()` për
ta kthyer në *integer* (numër të plotë).

### Zgjidhja 2

```python
for i in range(5):
    n = int(input())
    print(n * n)
```

https://tinyurl.com/101-prog-001-2

#### Sqarime

Duke përdorur një cikël **for** që përsëritet 5 herë, shmangim
përsëritjen në program të atyre dy rreshtave.

Vini re që `:` në fund të rreshtit `for...` tregojnë që më poshtë vjen
një *bllok* me *instruksione* (komanda, ose rreshta programi). Të
gjithë rreshtat e këtij blloku janë të spostuar në brendësi të
rreshtit `for...` me të njëjtin numër hapësirash. Ky bllok përsëritet
sa herë që ti thotë komanda `for...` (në rastin tonë, 5 herë).

Funksioni `range(5)` kthen një objekt që ka 5 vlera, siç tregon edhe
kjo provë:
```
$ python3
>>> list(range(5))
[0, 1, 2, 3, 4]
>>> quit()
$ _
```

### Detyra

Bëni një program që merr 5 numra dhe nxjerr kubin e tyre (fuqinë e tretë).
