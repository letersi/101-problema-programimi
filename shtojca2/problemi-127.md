### Kërkesa

Sergei ka bërë **N** matje dhe do të gjejë vlerën mesatare të tyre.
Por për të gjetur një mesatare sa më të mirë, ai mendon se duhet ti
heqë nga lista e matjeve **K** vlerat më të vogla dhe **K** vlerat më
të mëdha, para se të llogarisë mesataren. Sa del vlera mesatare në
këtë mënyrë?

Referenca: https://www.codechef.com/problems/SIMPSTAT

#### Shembull

```
$ cat input.txt
3
5 1
2 9 -10 25 1
5 0
2 9 -10 25 1
3 1
1 1 1

$ python3 prog.py < input.txt
4.000000
5.400000
1.000000
```

### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    n, k = map(int, input().split())
    A = list(map(int, input().split()))
    A.sort()
    s = 0
    i = k
    while i < n - k:
        s += A[i]
        i += 1
    print(s / (n - 2*k))
```

### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    n, k = map(int, input().split())
    A = list(map(int, input().split()))
    A.sort()
    s = 0
    for i in range(k, n - k):
        s += A[i]
    print(s / (n - 2*k))
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    n, k = map(int, input().split())
    A = list(map(int, input().split()))
    A.sort()
    print(sum(A[k:n-k]) / (n - 2*k))
```

