### Kërkesa

Shkruani një program që llogarit shumën e shifrave të një numri të
dhënë.

Referenca: https://www.codechef.com/problems/FLOW006

#### Shembull

```
$ cat input.txt
3
12345
31203
2123

$ python3 prog.py < input.txt
15
9
8
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    n = int(input())
    s = 0
    while n > 0:
        s += n % 10
        n = n // 10
    print(s)
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    n = input()
    s = 0
    for i in range(len(n)):
        s += int(n[i])
    print(s)
```

### Zgjidhja 3

```python
for _ in range(int(input())):
    n = input()
    s = 0
    for d in n: s += int(d)
    print(s)
```

### Detyra

Bëni një program që e shkruan mbrapsht një numër të dhënë natyror.

Referenca: https://www.codechef.com/problems/FLOW007

#### Shembull

```
$ cat input.txt
4
12345
31203
2123
2300

$ python3 prog.py < input.txt
54321
30213
3212
32
```

