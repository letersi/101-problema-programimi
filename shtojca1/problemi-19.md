### Kërkesa

Jepen 2 numra **a** dhe **n**. Bëni një program që gjen $`k = a^n`$,
pastaj merr shumën e shifrave të **k** dhe gjen nëse kjo shumë është
numër i thjeshtë ose jo.

Referenca: https://www.codechef.com/problems/GMPOW

#### Shembull

```
$ cat input.txt
2
2 4
5 3

$ python3 prog.py < input.txt
1
0
```

Në rastin e parë, $`2^4 = 16`$ dhe $`1 + 6 = 7`$ është numër i thjeshtë.

Në rastin e dytë, $`5^3 = 125`$ dhe $`1 + 2 + 5 = 8`$ nuk është numër
i thjeshtë.

### Zgjidhja

```python
import math

def is_prime(n):
    if n == 1: return False
    if n % 2 == 0 and n > 2: 
        return False
    for i in range(3, int(math.sqrt(n)) + 1, 2):
        if n % i == 0:
            return False
    return True

for _ in range(int(input())):
    a = int(input())
    n = int(input())
    k = a**n
    s = sum([int(d) for d in list(str(k))])
    print(1) if is_prime(s) else print(0)
```
