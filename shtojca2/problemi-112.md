### Kërkesa

Sa katrorë me madhësi **2x2** mund të futen në një trekëndësh kënddrejtë
dybrinjënjëshëm, ku kateti është me gjatësi **B**, dhe brinjët e
katrorëve janë paralel me katetet?

Referenca: https://www.codechef.com/problems/TRISQ

#### Shembull

```
$ cat input.txt
11
1
2
3
4
5
6
7
8
9
10
11

$ python3 prog.py < input.txt
0
0
0
1
1
3
3
6
6
10
10
```

### Zgjidhja

```python
for _ in range(int(input())):
    b = int(input())
    k = b // 2
    nr = k * (k - 1) // 2
    print(nr)
```

